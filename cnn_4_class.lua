----------------------------------------------------------------------
--cnn for layman
--u need to have a folder named 'data' paralle to where u place this code
--"data" folder consists four folder 1,2,3,4
--4 folder can have images(examples: cat,dogs,bird,fish) 
--in my case classes are bags,luggage,backpack,travel_accessories
--this code requires images to be 299X299 pixel(u can edit accordingly)
--ignore GetData function
--important part is making model and training model code
----------------------------------------------------------------------

require 'torch'
require 'nn'
require 'nnx'
require 'optim'
require 'image'
require 'pl'
require 'paths'
require 'torchx' -- for paths.indexdir

local debug = 1 --turn it to 0 to turn off print statements
imageDim = {299,299} --edit this according to ur input images

---------------------------------------------------------------------------------
--Section 1: Load data from folder containing images
---------------------------------------------------------------------------------
-- this function read image data from folders and save into tensors
-- should return tensor for data 
-- maybe like 2000 data points, each containing input : 3X299X299  and target : 1 class value
-- datapath shoud be data
-- -----------------------------------------------------------------------------
function GetData(dataPath,dataSize)

    local bags = paths.indexdir(paths.concat(dataPath, 'bags')) -- 1
    local luggage = paths.indexdir(paths.concat(dataPath, 'luggage')) -- 2
    local backpacks = paths.indexdir(paths.concat(dataPath, 'backpacks')) -- 3
    local travel_access = paths.indexdir(paths.concat(dataPath, 'travel_access')) -- 4
    local size = bags:size() + luggage:size() + backpacks:size() + travel_access:size() -- total number of images loaded
    if debug == 1 then
        print('total data' .. size)
        print('size of bags folder' .. bags:size())
        print('size of luggage folder' .. luggage:size())
        print('size of backpacks folder' .. backpacks:size())
        print('size of travel accessories folder' .. travel_access:size())
    end
    local shuffle = torch.randperm(size) -- shuffle the data
    local input = torch.FloatTensor(size, 3,imageDim[1] , imageDim[2])
    local target = torch.IntTensor(size):fill(4) -- default class 4

    print('loading bag data')
    for i=1,bags:size() do
        local img = image.load(bags:filename(i))
        local idx = shuffle[i]
        input[idx]:copy(img)
        target[idx] = 1
        collectgarbage()
        xlua.progress(i, bags:size())
    end

    print('loading luggage data')
    for i=1,luggage:size() do
        local img = image.load(luggage:filename(i))
        local idx = shuffle[i+bags:size()]
        input[idx]:copy(img)
        target[idx] = 2
        collectgarbage()
        xlua.progress(i, luggage:size())
    end

    print('loading backpack data')
    for i=1,backpacks:size() do
        local img = image.load(backpacks:filename(i))
        local idx = shuffle[i+bags:size()+luggage:size()]
        input[idx]:copy(img)
        target[idx] = 3
        collectgarbage()
        xlua.progress(i, backpacks:size())
    end

    print('loading travel accessories data')
    for i=1,travel_access:size() do
        local img = image.load(travel_access:filename(i))
        local idx = shuffle[i+bags:size()+luggage:size()+backpacks:size()]
        input[idx]:copy(img)
        target[idx] = 4
        collectgarbage()
        xlua.progress(i, travel_access:size())
    end
    --dataAll stores everything input and target arrays
    dataAll = {}
    dataAll.input = input
    dataAll.target = target
    return dataAll
    --TODO divide the data into training and test
end
----------------------------------------------------------------------------------  

----------------------------------------------------------------------------------
--default values --change according to needs
local network = 0 -- change it to 1 if u want to load old model
local batchSize = 10 --mini bacth size
local learnRate = 0.05
local momentum_ = 0
local save = 'results' --folder to save files
local L1 = 0 --coefficient for L1
local L2 = 0 --coefficient for L2
local plot = 1 -- 0 if u dont want to plot
----------------------------------------------------------------------

-- fix seed
torch.manualSeed(1)

-- threads
torch.setnumthreads(4)

-- use floats
torch.setdefaulttensortype('torch.FloatTensor')


----------------------------------------------------------------------
--4 class problem
--define as many as possible and edit accordingly
classes = {'bags','luggage','backpack','travel_access'}

----------------------------------------------------------------------
--Section 2: define neural network
----------------------------------------------------------------------
--function DefineNetwork()
if network == 0 then
    -- define model to train
    model = nn.Sequential()

    ------------------------------------------------------------
    -- convolutional layer 
    ------------------------------------------------------------
    --16 filters// 5X5 filter with stride 1 leads to 16X295X295
    --first param is depth of each 5X5 filter
   --first param = depth of the input tensor which is 3 for colored image r,g,b
   model:add(nn.SpatialConvolutionMM(3, 16, 5, 5)) -- 3X299X299 to 16X295X295

   model:add(nn.Tanh()) 

   --------------------------------------------------------------
   --pooling layer
   ---------------------------------------------------------------
   -- covert 3X3 to 1
   -- 3rd and 4rth arguments tells the step size like stride
   --  and last two arguments are padding
   --  16X295X295 leads to 16X((295 + 2*1 -3)/3+1)X((295 + 2*1 -3)/3+1) = 16X99X99
   -- -------------------------------------------------------------- 
   model:add(nn.SpatialMaxPooling(3, 3, 3, 3, 1, 1))--last two params are padding

   ------------------------------------------------------------
   -- convolutional layer 
   ------------------------------------------------------------
   -- first param is depth of each 5X5 filter
   --first param = depth of the input tensor, which is 16 in this case
   --input = 16X99X99
   ------------------------------------------------------------
   model:add(nn.SpatialConvolutionMM(16, 32, 5, 5)) -- 16X99X99 to 32X95X95

   model:add(nn.Tanh())
   --------------------------------------------------------------
   --pooling layer
   ---------------------------------------------------------------
   -- covert 5X5 to 1
   -- 3rd and 4rth arguments tells the step size like stride
   --  no padding
   --  32X95X95 leads 32X19X19
   ---------------------------------------------------------------
   model:add(nn.SpatialMaxPooling(5, 5, 5, 5))

   ------------------------------------------------------------
   -- convolutional layer 
   ------------------------------------------------------------
   -- first param is depth of each 5X5 filter
   --first param = depth of the input tensor, which is 32 in this case
   --input = 32X19X19
   ------------------------------------------------------------
   model:add(nn.SpatialConvolutionMM(32, 32, 5, 5)) --32X19X19 to 32X15X15

   model:add(nn.Tanh())

   --------------------------------------------------------------
   --pooling layer
   ---------------------------------------------------------------
   -- covert 2X2 to 1
   -- 3rd and 4rth arguments tells the step size like stride
   --  no padding
   --  32X15X15 leads 32X5X5
   ---------------------------------------------------------------
   model:add(nn.SpatialMaxPooling(3, 3, 3, 3))

   ---------------------------------------------------------------
   -- now starts fully connected network
   -- reshape just changes 2d input to 1d long vector
   -- 32X5X5 changes to 800X1
   model:add(nn.Reshape(32*5*5))
   -- linear layer changes 800 tp 100 (100 neurons in hidden layer)
   model:add(nn.Linear(32*5*5, 100))
   model:add(nn.Tanh())
   --100 to 4
   model:add(nn.Linear(100, #classes))
   --logsoftmax layer is added later
   ------------------------------------------------------------
else
    --reload the model
    model = torch.load("results/model.net")

end

-- retrieve parameters and gradients
parameters,gradParameters = model:getParameters()

-- printing the model
print('classification using model:')
print(model)

----------------------------------------------------------------------
-- loss function: negative log-likelihood
-- log( exp(x*theta_i) / sum_j(exp(x*theta_j)) 
model:add(nn.LogSoftMax())
--NLL => negative log likehood
criterion = nn.ClassNLLCriterion()
--end
-----------------------------------------------------------
-- this matrix records the current confusion across classes
confusion = optim.ConfusionMatrix(classes)
-- confusion matrix is nclassXnclass matrix
-- add(predition,target) adds 1 to ij element of d matrix
-- where i= prediction and j= target
-- if prediction = target 1 is added to diagonl
-- dense digonal means less confusion
----------------------------------------------------------

-- log results to files
trainLogger = optim.Logger(paths.concat(save, 'train_accuracy.log'))
testLogger = optim.Logger(paths.concat(save, 'test_accuracy.log'))

---------------------------------------------------------------
--Section 3: Train the neural network
---------------------------------------------------------------
-- training function
-- dataset contains input tensor and target array
-- comes from function GetData
function Train(dataset,trainSize)
    -- epoch tracker
    epoch = epoch or 1

    -- do one epoch
    print('Learning using training set:')
    print("epoch # " .. epoch .. ' [batchSize = ' .. batchSize .. ']')
    --print('size of dataset is' .. dataset.target:size())
    local datasetSize = trainSize 
    for t = 1,datasetSize,batchSize do
        -- create mini batch
        local inputs = torch.Tensor(batchSize,3,imageDim[1],imageDim[2])
        local targets = torch.Tensor(batchSize)
        local k = 1
        for i = t,math.min(t+batchSize-1,datasetSize) do
            inputs[k] = dataset.input[i]-- input means 3X299X299
            targets[k] = dataset.target[i]-- target is just scalar element
            k = k + 1
        end

        -- create closure to evaluate f(X) and df/dX
        local feval = function(x)
            collectgarbage()

            -- get new parameters
            if x ~= parameters then
                parameters:copy(x)
            end

            -- reset gradients
            gradParameters:zero()

            -- evaluate function for complete mini batch
            local outputs = model:forward(inputs)
            local f = criterion:forward(outputs, targets)

            -- estimate df/dW
            local df_do = criterion:backward(outputs, targets)
            model:backward(inputs, df_do)

            -- penalties (L1 and L2):
            if L1 ~= 0 or L2 ~= 0 then
                -- locals:
                local norm,sign= torch.norm,torch.sign

                -- Loss:
                f = f + L1 * norm(parameters,1)
                f = f + L2 * norm(parameters,2)^2/2

                -- Gradients:
                gradParameters:add( sign(parameters):mul(L1) + parameters:clone():mul(L2) )
            end

            -- update confusion
            for i = 1,batchSize do
                confusion:add(outputs[i], targets[i])
            end

            -- return f and df/dX
            return f,gradParameters
        end

        --Stochastic gradient descent 
        -- Perform SGD step:
        sgdState = sgdState or {
            learningRate = learnRate,
            momentum = momentum_,
            learningRateDecay = 5e-7
        }
        optim.sgd(feval, parameters, sgdState)

        -- disp progress
        xlua.progress(t, datasetSize)
    end

    -- print confusion matrix
    print(confusion)

    --image.display(confusion:render()) -- todo make it work
    trainLogger:add{['% Average Accuracy (training set)'] = confusion.totalValid * 100}
    confusion:zero()

    -- save/log current net
    local filename = paths.concat(save, 'model.net')
    os.execute('mkdir -p ' .. sys.dirname(filename))
    if paths.filep(filename) then
        os.execute('mv ' .. filename .. ' ' .. filename .. '.old')
    end
    print('saving network to '..filename)
    torch.save(filename, model)

    -- next epoch
    epoch = epoch + 1
end

---------------------------------------------------------------------
--Section 4 : Test the neural network
---------------------------------------------------------------------
-- test function
-- takes full data set and runs it from index(+1) where training stopped
function Test(dataset,testIndex)

    -- test over given dataset
    print('<trainer> on testing Set:')
   local datasetSize = dataset.target:size()[1] --first element is the value of size
   local a = testIndex -- 1601 -- should be 1 todo
   print(a)
   print(datasetSize)
   for t = a,datasetSize,batchSize do
      -- disp progress
      xlua.progress(t, datasetSize)

      -- create mini batch
      local inputs = torch.Tensor(batchSize,3,imageDim[1],imageDim[2])
      local targets = torch.Tensor(batchSize)
      local k = 1
      for i = t,math.min(t+batchSize-1,datasetSize) do
         inputs[k] = dataset.input[i]-- input means 3X299X299
         targets[k] = dataset.target[i]-- target is just scalar element
         k = k + 1
      end

      -- test samples
      local preds = model:forward(inputs)

      -- confusion:
      for i = 1,batchSize do
         confusion:add(preds[i], targets[i])
      end
   end

   -- print confusion matrix
   print(confusion)
   testLogger:add{['% Average Accuracy (testing set)'] = confusion.totalValid * 100}
   confusion:zero()
end

---------------------------------------------------------------------
--Section 5: main function
----------------------------------------------------------------------
function main()
    --assign the path of data folder -- it starts from folder where u have this .lua file
    path_dataset = 'data' --change the name according to ur folder which contains data
    --load data
    trainData = GetData(path_dataset) -- nX3X299X299
    --print(trainData.input[1])--debug
    --print(trainData.target[1])--debug
    local size1 = trainData.target:size()
    local trainSize = torch.floor(size1[1]*8/100)*10 -- use 80% data for training and rest for testing
    local testStartIndex = trainSize + 1 --todo smart division of data

    if debug == 1 then
        print(size1)
        print(trainSize)
        print(testStartIndex)
    end

    count = 0
    --running for 10 epochs
    --stop according to some criteria
    while count < 10 do
        -- trainig and testing
        Train(trainData,trainSize)
        Test(trainData,testStartIndex)-- testing is done on 20% data
        count = count +1;
    end

    -- plot errors
    if plot then
        trainLogger:style{['% Average Accuracy (training set)'] = '-'}
        testLogger:style{['% Average Accuracy (testing set)'] = '-'}
        trainLogger:plot()
        testLogger:plot()
    end
end

main()