# Image Classification using CNN
This projects is about image recognition using COnvolution Neural Network.

I have used 4 classes to demonstrate it.

4 classes can be anthing like cat,dog,bird,fish

There are enough comments in the code to explain stuff.

## Requirments 

Torch7 to be installed, as well as some extra (3rd-party) packages.

## Installing

[Install torch](http://torch.ch/docs/getting-started.html)

simply try to install packages using luarocks:
```shell
$ luarocks install image    # an image library for Torch7
$ luarocks install nnx      # lots of extra neural-net modules
```
## Input data
There should be a folder named 'data' at the location where you place this .lua file

Data folder contains 4 folder (4 classes) (edit code according to your folder names and image data)

## Code Description
There is just one file **cnn_4_class.lua**

It has 5 sections:

Section 1: loading data from folders. (folders should contain images 299X299 in my case).

Section 2: Define neural network.

Section 3: Training of neural network.

Section 4: Testing of neural network.

Section 5: Main function which call above functions.

## How to run
open terminal at the location where lua file is located.

type: th cnn_4_class.lua 

## Applications

Learning features of any class of images 

*example* [Learning features of good selfies and bad selfies](http://karpathy.github.io/2015/10/25/selfie/)

## Lots of comments in the code to explain stuff

# *my first project on gitlab. correct me if I am wrong on anything 